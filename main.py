from turtle import Screen, Turtle
from paddle import Paddle
import time
from ball import Ball
from scoreboard import Scoreboard

WIDTH = 800
HEIGHT = 600
MIDLINE_POSITIONS = [(0, 280), (0, 250), (0, 220), (0, 190), (0, 160), (0, 130), (0, 100), (0, 70), (0, 40), (0, 9),
                     (0, -22), (0, -53), (0, -84), (0, -115), (0, -146), (0, -177), (0, -208), (0, -239), (0, -270)]

# 1. create the screen / initialize background
screen = Screen()
screen.setup(WIDTH, HEIGHT)
screen.bgcolor("black")
screen.tracer(0)
for position in MIDLINE_POSITIONS:
    new_line = Turtle(shape="square")
    new_line.shapesize(stretch_wid=1.1, stretch_len=0.1)
    new_line.color("white")
    new_line.penup()
    new_line.goto(position)

screen.listen()
# 2a. initialize left paddle
left_paddle = Paddle("left")
screen.onkey(left_paddle.up, "w")
screen.onkey(left_paddle.down, "s")
# 2b. initialize right paddle
right_paddle = Paddle("right")
screen.onkey(right_paddle.up, "Up")
screen.onkey(right_paddle.down, "Down")

ball = Ball()
scoreboard = Scoreboard()

game_is_on = True
while game_is_on:
    # update screen every 0.1 seconds, move paddles automatically
    screen.update()
    time.sleep(0.05)
    left_paddle.move()
    right_paddle.move()
    ball.move()

    # keep paddle in boundaries of the screen
    if left_paddle.head.distance((-380, 300)) < 10:
        left_paddle.down()
    if left_paddle.tail.distance((-380, -300)) < 10:
        left_paddle.up()
    if right_paddle.head.distance((370, 300)) < 10:
        right_paddle.down()
    if right_paddle.tail.distance((370, -300)) < 10:
        right_paddle.up()

    # detect collision with walls
    if ball.ycor() > 290 or ball.ycor() < -290:
        ball.setheading(ball.bounce())

    # detect collision with paddle
    for segment in left_paddle.segments:
        if ball.distance(segment) < 20:
            ball.setheading(ball.bounce_paddle())
    for segment in right_paddle.segments:
        if ball.distance(segment) < 20:
            ball.setheading(ball.bounce_paddle())

    # paddle misses. collision with side walls
    if ball.xcor() > 395:
        # left player scores
        scoreboard.score_up("left")
        scoreboard.show_score()

        if scoreboard.left_score > 2:
            scoreboard.game_over()
            game_is_on = False
        else:
            ball.refresh()
            pass

    if ball.xcor() < -395:
        # right player scores
        scoreboard.score_up("right")
        scoreboard.show_score()

        if scoreboard.right_score > 2:
            scoreboard.game_over()
            game_is_on = False
        else:
            ball.refresh()
            pass


screen.exitonclick()




