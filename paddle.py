from turtle import Turtle

LEFT_STARTING_POSITIONS = [(-380, 0), (-380, 20), (-380, 40), (-380, -20), (-380, -40)]
RIGHT_STARTING_POSITIONS = [(370, 0), (370, 20), (370, 40), (370, -20), (370, -40)]
MOVE_DISTANCE = 15
UP = 90
DOWN = 270


class Paddle():
    def __init__(self, side):
        #super().__init__()
        self.side = side
        self.segments = []
        self.create_paddle()
        self.head = self.segments[0]
        self.tail = self.segments[-1]
        self.head.setheading(UP)
        self.tail.setheading(UP)

    def create_paddle(self):
        if self.side == "left":
            for position in LEFT_STARTING_POSITIONS:
                self.add_segment(position)
        elif self.side == "right":
            for position in RIGHT_STARTING_POSITIONS:
                self.add_segment(position)

    def add_segment(self, position):
        segment = Turtle(shape="square")
        segment.color("white")
        segment.penup()
        segment.setposition(position)
        self.segments.append(segment)

    def move(self):
        if self.head.heading() == UP:
            for seg_num in range(len(self.segments)-1, 0, -1):
                new_x = self.segments[seg_num - 1].xcor()
                new_y = self.segments[seg_num - 1].ycor()
                self.segments[seg_num].goto(new_x, new_y)
            self.head.forward(MOVE_DISTANCE)

        elif self.head.heading() == DOWN:
            for seg_num in range(0, len(self.segments) - 1):
                new_x = self.segments[seg_num + 1].xcor()
                new_y = self.segments[seg_num + 1].ycor()
                self.segments[seg_num].goto(new_x, new_y)
            self.tail.forward(MOVE_DISTANCE)

    def up(self):
        self.head.setheading(UP)
        self.tail.setheading(UP)

    def down(self):
        self.head.setheading(DOWN)
        self.tail.setheading(DOWN)
