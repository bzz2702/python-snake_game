from turtle import Turtle

class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.left_score = 0
        self.right_score = 0
        self.show_score()

    def show_score(self):
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(0, 240)
        self.write(f"{self.left_score}    {self.right_score}", True, align="center", font=('Arial', 40, 'normal'))

    def score_up(self, player):
        self.clear()
        if player == "left":
            self.left_score += 1
        elif player == "right":
            self.right_score += 1

    def game_over(self):
        self.goto(0, 0)
        if self.left_score > self.right_score:
            self.write(f"Left player wins!", True, align="center", font=('Arial', 25, 'normal'))
        else:
            self.write(f"Right player wins!", True, align="center", font=('Arial', 25, 'normal'))