import random
from turtle import Turtle

MOVE_DISTANCE = 10
START_DIRECTIONS = [45, 135, 225, 315]


class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("square")
        self.color("white")
        self.penup()
        self.speed("fastest")
        angle = random.choice(START_DIRECTIONS)
        self.setheading(angle)
        self.tiltangle(-45)

    def move(self):
        self.forward(MOVE_DISTANCE)

    def bounce(self):
        if self.heading() == 45:
            return 315
        elif self.heading() == 315:
            return 45
        elif self.heading() == 135:
            return 225
        elif self.heading() == 225:
            return 135

    def bounce_paddle(self):
        if self.heading() == 45:
            return 135
        elif self.heading() == 315:
            return 225
        elif self.heading() == 135:
            return 45
        elif self.heading() == 225:
            return 315

    def refresh(self):
        self.goto(0, 0)